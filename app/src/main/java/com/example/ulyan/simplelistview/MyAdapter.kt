package com.example.ulyan.simplelistview

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.*

import android.util.Log
import android.util.SparseBooleanArray

/**
 * Created by ulyan on 31.10.2016.
 */
class MyAdapter() : RecyclerView.Adapter<MyAdapter.MyViewHolder>(), Parcelable {

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeList(data)
        dest?.writeSparseBooleanArray(selectedItems)
    }

    companion object {
        @JvmField final val CREATOR: Parcelable.Creator<MyAdapter> = object : Parcelable.Creator<MyAdapter> {
            override fun createFromParcel(source: Parcel): MyAdapter{
                return MyAdapter(source)
            }

            override fun newArray(size: Int): Array<MyAdapter?> {
                return arrayOfNulls(size)
            }
        }
    }


    interface DrawableProvider{
        fun getDrawableByID(id : Int) : Drawable
    }

    class MyViewHolder : RecyclerView.ViewHolder, View.OnClickListener, View.OnLongClickListener{

        interface ClickListener{
            fun onItemClicked(position : Int)
            fun onItemLongClick(position : Int) : Boolean
        }

        var title: TextView? = null
        var text: TextView? = null
        var image: View? = null
        var listener: ClickListener? = null

        constructor(holderView : View, clickListener : ClickListener) : super(holderView){
            title = holderView.findViewById(R.id.item_title) as TextView
            text = holderView.findViewById(R.id.item_text) as TextView
            image = holderView.findViewById(R.id.item_image) as View
            listener = clickListener

            holderView.isSelected = false

            holderView.setOnClickListener(this)
            holderView.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            listener?.onItemClicked(layoutPosition)
        }

        override fun onLongClick(v: View?): Boolean {
            if(listener != null){
                return listener!!.onItemLongClick(layoutPosition)
            }
            return false
        }
    }

    var data: ArrayList<Record> = ArrayList<Record>()
    private var selectedItems = SparseBooleanArray()
    lateinit var dbh : DataBaseHelper
    lateinit var db : SQLiteDatabase
    lateinit var c : Cursor
    lateinit var clickListener: MyViewHolder.ClickListener
    lateinit var drawableProvider : DrawableProvider
    lateinit var context : Context

    constructor(clickListener: MyViewHolder.ClickListener,
                drawableProvider : DrawableProvider, context : Context) : this(){
        this.clickListener = clickListener
        this.drawableProvider = drawableProvider
        this.context = context
        try {
            dbh = DataBaseHelper(context)
            db = dbh.readableDatabase
            c = db.query("recordTable", null, null, null, null, null, null)
            Log.w("Load","start")
            if (c.moveToFirst()) {
                do {
                    var record = Record(
                            c.getLong(c.getColumnIndex("recordId")),
                            c.getString(c.getColumnIndex("recordTitle")),
                            c.getString(c.getColumnIndex("recordText")),
                            c.getInt(c.getColumnIndex("imageId")))
                    data.add(record)
                    Log.w("Load","success")
                } while (c.moveToNext())
            }
        }
        catch (ex : SQLiteException){}
    }

    constructor(clickListener: MyViewHolder.ClickListener,
                drawableProvider : DrawableProvider, context : Context, selectedItems : SparseBooleanArray,
                data : ArrayList<Record>) : this(){
        this.clickListener = clickListener
        this.drawableProvider = drawableProvider
        this.selectedItems = selectedItems
        this.data = data
        this.context = context
    }
    constructor(instream: Parcel):this(){
        data = ArrayList<Record>()
        instream.readList(data,null)
        selectedItems = instream.readSparseBooleanArray()
    }

    val selectedItemCount : Int
        get() = selectedItems.size()

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_layout, parent, false)
        return MyViewHolder(itemView, clickListener)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        if(holder != null){
            val record = data.get(position)
            holder.title?.setText(record.title)
            holder.text?.setText(record.text)
            holder.image?.background = drawableProvider.getDrawableByID(record.imageID)

            holder.itemView.isSelected = selectedItems.get(position, false)
            holder.image?.setOnClickListener { holder.listener?.onItemLongClick(position) }
        }
    }

    fun toggleSelection(position : Int) {
        if (selectedItems.get(position, false)) {
            selectedItems.delete(position)
        } else {
            selectedItems.put(position, true)
        }
        notifyItemChanged(position)
    }

    val selectedItemsList : ArrayList<Int>
        get() {val items = ArrayList<Int>(selectedItems.size());
            for (i in 0..selectedItems.size()-1) {
                items.add(selectedItems.keyAt(i))
            }
            return items}

    fun clearSelection() {
        var selection = selectedItemsList
        selectedItems.clear()
        for ( i in selection) {
            notifyItemChanged(i)
        }
    }

    fun getItem(position: Int) : Record{
        return data.get(position)
    }

    fun removeItem(position : Int){
        Log.w("Delete","start")
        try{
            db = dbh.writableDatabase
            var cv = ContentValues()
            Log.w("Delete","success")
            db.delete("recordTable", "recordId=?", arrayOf(data.get(position).id.toString()))
        }
        catch (ex : SQLiteException){ }
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    fun removeItems(){
        var selection = selectedItemsList
        selectedItems.clear()
        for ( i in selection.size-1 downTo 0) {
            removeItem(selection.get(i))
        }
        notifyDataSetChanged()
    }

    fun select_all(){
        if(selectedItemsList.size == data.size){
            clearSelection()
        }
        else{
            for(i in 0..data.size-1){
                if (selectedItems.get(i, false) == false) {
                    selectedItems.put(i, true)
                }
            }
        }
        notifyDataSetChanged()
    }

    fun add(newRecord : Record) : Int{
        try {
            db = dbh.writableDatabase
            var cv = ContentValues()
            cv.put("recordTitle", newRecord.title)
            cv.put("recordText", newRecord.text)
            cv.put("imageId", newRecord.imageID)
            var Id = db.insert("recordTable", null, cv)
            Log.w("Insert", data.indexOf(newRecord).toString())
            var toAdd = Record(Id,newRecord.title,newRecord.text,newRecord.imageID)
            data.add(toAdd)
            notifyDataSetChanged()
            return data.indexOf(toAdd)
        }
        catch (ex : SQLiteException){
            Log.w("Insert", "failed")
            return -1
        }

    }

    fun update(record : Record){
        try {
            db = dbh.writableDatabase
            var cv = ContentValues()
            cv.put("recordTitle", record.title)
            cv.put("recordText", record.text)
            cv.put("imageId", record.imageID)
            db.update("recordTable",cv, "recordId=?", arrayOf(record.id.toString()))
            Log.w("Update", "success")
            notifyItemChanged(data.indexOf(record))
        }
        catch (ex : SQLiteException){ Log.w("Update", ex.toString())}
    }
}