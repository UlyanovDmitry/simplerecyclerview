package com.example.ulyan.simplelistview

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by ulyan on 02.11.2016.
 */
class DataBaseHelper(context : Context) : SQLiteOpenHelper(context, "RecordDataBase", null, 1){
    override fun onCreate(db: SQLiteDatabase?) {
        if(db != null){
            db.execSQL("create table recordTable ("
                    + "recordId integer primary key autoincrement,"
                    + "recordTitle text,"
                    + "recordText text,"
                    + "imageId integer" + ");")
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}