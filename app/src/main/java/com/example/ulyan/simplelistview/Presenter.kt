package com.example.ulyan.simplelistview

import java.util.*

/**
 * Created by ulyan on 31.10.2016.
 */
class Presenter(val mainActivity: MainActivity){
    val recordGenerator = RecordGenerator()
    fun getRecords() : ArrayList<Record> {
        return recordGenerator.generateRecords()
    }

    fun getNewRecord():Record{
        return recordGenerator.generateSingleRecord()
    }

    var MaxId : Int
        get() = recordGenerator.id
        set(value) {
            recordGenerator.id = value
        }
}