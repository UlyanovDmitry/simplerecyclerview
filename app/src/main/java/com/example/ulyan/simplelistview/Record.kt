package com.example.ulyan.simplelistview

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by ulyan on 31.10.2016.
 */
data class Record(public val id : Long, public var title : String, public var text : String, public var imageID : Int) : Parcelable{

    constructor(instream: Parcel) : this(instream.readLong(), instream.readString(),instream.readString(), instream.readInt()){ }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeLong(id)
        dest?.writeString(title)
        dest?.writeString(text)
        dest?.writeInt(imageID)
    }

    companion object {
        @JvmField final val CREATOR: Parcelable.Creator<Record> = object : Parcelable.Creator<Record> {
            override fun createFromParcel(source: Parcel): Record{
                return Record(source)
            }

            override fun newArray(size: Int): Array<Record?> {
                return arrayOfNulls(size)
            }
        }
    }
}