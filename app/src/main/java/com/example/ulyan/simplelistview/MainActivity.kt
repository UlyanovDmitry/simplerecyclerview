package com.example.ulyan.simplelistview

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.view.ActionMode
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.SparseArray
import android.view.*
import android.widget.Toast
import java.util.*

const val IMAGESCOUNT = 8

class MainActivity : AppCompatActivity(), MyAdapter.MyViewHolder.ClickListener, MyAdapter.DrawableProvider  {

    inner class ActionModeCallBack() : ActionMode.Callback{
        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
            if(item != null){
                when(item.itemId){
                    R.id.cab_delete -> {
                        adapter.removeItems()
                        mode?.finish()
                        return true
                    }
                    R.id.cab_select_all ->{
                        adapter.select_all()
                        val count = adapter.selectedItemCount
                        if(count == 0){
                            mode?.finish()
                        }
                        else{
                            mode?.setTitle(count.toString())
                            mode?.invalidate()
                        }
                        return true
                    }
                    else -> {
                        return false
                    }
                }
            }
            return true
        }

        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            if(mode != null && menu != null){
                mode.menuInflater.inflate(R.menu.context_menu,menu)
                return true
            }
            return true
        }

        override fun onDestroyActionMode(mode: ActionMode?) {
            adapter.clearSelection()
            actionMode = null
        }
    }
    val presenter = Presenter(this)
    lateinit private var data : ArrayList<Record>
    lateinit var recyclerView : RecyclerView
    var actionMode : ActionMode? = null
    lateinit var actionModeCallback : ActionModeCallBack
    lateinit var adapter : MyAdapter
    var imageArray : SparseArray<Drawable>? = null
    var recordDialog : RecordDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(savedInstanceState != null && savedInstanceState.containsKey("MaxID")){
            presenter.MaxId = savedInstanceState.getInt("MaxID")
        }
        if(savedInstanceState == null || !savedInstanceState.containsKey("Adapter")) {
            adapter = MyAdapter(this, this, this)
        }
        else{
            adapter = savedInstanceState.getParcelable("Adapter")
            adapter.clickListener = this
            adapter.drawableProvider = this
            adapter.context = this
        }

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.scrollToPosition(0)

        imageArray = SparseArray<Drawable>()
        for(i in 0..IMAGESCOUNT-1){
            imageArray?.put(i,this.getResources().getDrawable(resources.getIdentifier("image_id$i", "drawable",
                    getPackageName())))
        }

        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        recyclerView.layoutManager = layoutManager
        actionModeCallback = ActionModeCallBack()
        recyclerView.adapter = adapter
        recyclerView.itemAnimator = DefaultItemAnimator()

        if(adapter.selectedItemCount > 0){
            actionMode = startSupportActionMode(actionModeCallback)
            actionMode?.title = adapter.selectedItemCount.toString()
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt("MaxID", presenter.MaxId)
        outState?.putParcelable("Adapter", adapter)
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    fun toogleSelection(position : Int){
        adapter.toggleSelection(position)

        val count = adapter.selectedItemCount
        if(count == 0){
            actionMode?.finish()
            actionMode = null
        }
        else{
            actionMode?.setTitle(count.toString())
            actionMode?.invalidate()
        }
    }

    override fun onItemClicked(position: Int) {
        if(actionMode == null) {
            recordDialog = RecordDialog()
            if(recordDialog != null){
                recordDialog!!.position = position
                recordDialog!!.adapter = adapter
                recordDialog!!.show(fragmentManager,"TestDialog")
            }
            /*Toast.makeText(getApplicationContext(),
                    resources.getString(R.string.on_item_click) + position.toString(), Toast.LENGTH_SHORT).show()*/
        }
        else{
            toogleSelection(position)
        }
    }

    override fun onItemLongClick(position: Int): Boolean {
        if(actionMode == null){
            actionMode = startSupportActionMode(actionModeCallback)
        }
        toogleSelection(position)
        return true
    }

    fun add_item(menuItem: MenuItem){
        var newRecord = presenter.getNewRecord()
        recordDialog = RecordDialog()
        if(recordDialog != null){
            recordDialog!!.position = adapter.add(newRecord)
            recordDialog!!.adapter = adapter
            recordDialog!!.show(fragmentManager,"TestDialog")
        }
    }

    fun changeImage(record : Record){
        record.imageID = (record.imageID + 1) % IMAGESCOUNT
    }

    override fun getDrawableByID(id : Int) : Drawable{
        val defaultDrawable = this.getResources().getDrawable(R.drawable.default_image)
        if(imageArray != null && imageArray!!.get(id, defaultDrawable) != defaultDrawable){
            return imageArray!!.get(id)
        }
        else{
            return defaultDrawable
        }
    }

}