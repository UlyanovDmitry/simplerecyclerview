package com.example.ulyan.simplelistview

import android.app.DialogFragment
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.text.InputType
import android.view.View
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import java.util.*

/**
 * Created by ulyan on 02.11.2016.
 */
class RecordDialog() : DialogFragment() {

    var position : Int = -1
    var adapter : MyAdapter? = null
    lateinit var titleView : TextView
    lateinit var textView : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(savedInstanceState != null && savedInstanceState.containsKey("Position")) {
            position = savedInstanceState.getInt("Position")
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.setTitle(R.string.dialog_title)

        if(position > -1) {
            adapter = (activity as MainActivity).adapter
        }

        val view = inflater!!.inflate(R.layout.record_dialog, null)

        view.findViewById(R.id.on_save_button).setOnClickListener { onSave() }
        view.findViewById(R.id.on_cancel_button).setOnClickListener { onCancel() }

        titleView = view.findViewById(R.id.editTitle) as TextView
        titleView.isCursorVisible = true
        titleView.isFocusableInTouchMode = true
        titleView.inputType = InputType.TYPE_CLASS_TEXT
        titleView.requestFocus()

        textView = view.findViewById(R.id.editText) as TextView
        textView.isCursorVisible = true
        textView.isFocusableInTouchMode = true
        textView.inputType = InputType.TYPE_CLASS_TEXT
        textView.requestFocus()

        val image = view.findViewById(R.id.dialog_item_image) as ImageView

        val record = adapter?.getItem(position)

        if(record != null){
            titleView.setText(record!!.title)
            textView.setText(record!!.text)
            image.setImageDrawable(resources.getDrawable(resources.getIdentifier("image_id${record!!.imageID}", "drawable",
                    activity.getPackageName())))
            image.setOnClickListener { (activity as MainActivity).changeImage(record); adapter?.notifyItemChanged(position);
                image.setImageDrawable(resources.getDrawable(resources.getIdentifier("image_id${record!!.imageID}", "drawable",
                        activity.getPackageName())))}
        }
        return view!!
    }


    fun onSave(){
        val record = adapter?.getItem(position)
        if(record != null){
            record!!.title = titleView.text.toString()
            record!!.text = textView.text.toString()
            adapter?.update(record)
        }
        super.onDismiss(dialog)
    }

    fun onCancel(){
        super.onDismiss(dialog)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt("Position", position)
        super.onSaveInstanceState(outState)
    }
}