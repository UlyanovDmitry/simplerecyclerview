package com.example.ulyan.simplelistview

import java.util.*

/**
 * Created by ulyan on 31.10.2016.
 */
class RecordGenerator {
    private val randomGenerator = Random()
    var id = 0
    private val texts = arrayOf("Nicolas Cage", "Yes, it's Nicolas too", "Yeah, it is Nicolas again",
            "Those who cannot be called", "Veeeeeeeeeeeeeeeeeeeeeeeeryyyyyyyyyyyyyyyyyyyy looooooooooooong string")
    fun generateSingleRecord() : Record {
        id++
        return Record(-1,"Nicolas Cage #$id",texts[randomGenerator.nextInt(texts.size)], randomGenerator.nextInt(IMAGESCOUNT))
    }

    fun generateRecords() : ArrayList<Record> {
        val size = randomGenerator.nextInt(10) + 5
        var result = ArrayList<Record>()
        for(i in 0..size-1){
            result.add(generateSingleRecord())
        }
        return result
    }
}